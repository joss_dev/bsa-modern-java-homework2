package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public class Library {

	private final String name;

	private final List<Library> dependencies;

	public Library(String name) {
		this.name = name;
		this.dependencies = new ArrayList<>();
	}

	public String getName() {
		return this.name;
	}

	public void addToDependency(Library dependency) {
		this.dependencies.add(dependency);
	}

	public List<Library> getDependencies() {
		return this.dependencies;
	}

}
