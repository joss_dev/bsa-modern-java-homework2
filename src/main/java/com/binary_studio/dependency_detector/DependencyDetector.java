package com.binary_studio.dependency_detector;

import java.util.*;
import java.util.stream.Collectors;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if (libraries.libraries == null || libraries.dependencies == null) {
			return true;
		}
		return getLibraries(libraries).stream().allMatch(DependencyDetector::extracted);
	}

	private static boolean extracted(Library lib) {
		Set<String> visitedLibs = new HashSet<>();
		Deque<Library> stack = new ArrayDeque<>();

		stack.add(lib);

		while (!stack.isEmpty()) {
			Library pop = stack.pop();

			if (visitedLibs.contains(pop.getName())) {
				return false;
			}

			visitedLibs.add(pop.getName());
			stack.addAll(pop.getDependencies());
		}

		return true;
	}

	private static Collection<Library> getLibraries(DependencyList libraries) {

		Map<String, Library> mapLibraries = libraries.libraries.stream().map(Library::new)
				.collect(Collectors.toMap(Library::getName, library -> library));

		libraries.dependencies.forEach(dependency -> {
			Library library = mapLibraries.get(dependency[0]);
			Library libraryDependency = mapLibraries.get(dependency[1]);

			if (library != null && libraryDependency != null) {
				library.addToDependency(libraryDependency);
			}
		});
		return mapLibraries.values();
	}

}
