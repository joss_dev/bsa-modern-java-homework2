package com.binary_studio.academy_coin;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {

		AtomicInteger prev = new AtomicInteger(Integer.MAX_VALUE);

		return prices.reduce(0, (acc, value) -> {
			int diff = Math.max(value - prev.get(), 0);
			prev.set(value);
			return diff + acc;
		});
	}

}
