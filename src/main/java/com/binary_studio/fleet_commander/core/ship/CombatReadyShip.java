package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger maxShiedHP;

	private final PositiveInteger maxHullHP;

	private final PositiveInteger capacitorMaxAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.maxShiedHP = shieldHP;
		this.hullHP = hullHP;
		this.maxHullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorMaxAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger.of(Math.min(
				this.capacitorAmount.value() + this.capacitorRechargeRate.value(), this.capacitorMaxAmount.value()));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
		final AttackAction attackAction = new AttackAction(this.attackSubsystem.attack(target), this, target,
				this.attackSubsystem);
		return Optional.of(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		final int damage = this.defenciveSubsystem.reduceDamage(attack).damage.value();
		final int hullDamage = this.shieldHP.value() < damage ? damage - this.shieldHP.value() : 0;
		this.shieldHP = PositiveInteger.of(Math.max(this.shieldHP.value() - damage, 0));
		this.hullHP = PositiveInteger.of(Math.max(this.hullHP.value() - hullDamage, 0));

		if (this.hullHP.value() == 0) {
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damage), this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		final RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		int newHullHP = Math.min(this.hullHP.value() + regenerateAction.hullHPRegenerated.value(),
				this.maxHullHP.value());
		int newShieldHP = Math.min(this.shieldHP.value() + regenerateAction.shieldHPRegenerated.value(),
				this.maxShiedHP.value());
		final RegenerateAction cappedAction = new RegenerateAction(
				PositiveInteger.of(newShieldHP - this.shieldHP.value()),
				PositiveInteger.of(newHullHP - this.hullHP.value()));
		return Optional.of(cappedAction);
	}

}
